module.exports = {
  devServer: {
    disableHostCheck: true,
    overlay: {
      warnings: false,
      errors: false
    }    
  },

  transpileDependencies: ['vuetify'],

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false
    }
  }

  // css: {
  //   loaderOptions: {
  //     sass: {
  //       data: `
  //         @import "@/scss/_variables.scss";
  //       `
  //     }
  //   }
  // }
};
