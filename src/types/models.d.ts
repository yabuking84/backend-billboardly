export type Role = '1' | '2';

export interface AxiosIntrfc {
  config: {
    headers: {
      Authorization: string | null;
    };
  };
}

export interface Auth_user {
  name: string | null;
  firstname: string | null;
  lastname: string | null;
  email: string | null;
  avatar: string | null;
  role: string | null;
  uuid: string | null;
  job_title: string | null;
  phone: string | null;
  fax: string | null;
  address: string | null;
  country_id: string | null;
}


export interface Api {
  method: string;
  url: string;
}



export interface User {
    email: string;
    password: string;    
    first_name: string;
    last_name: string;
}
