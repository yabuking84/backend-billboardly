import app from './app';
import auth from './auth';

export default {
  auth,
  app
};
