const role = {
  admin: {
    id: 1
  },
  partner: {
    id: 2
  }
};

const roleIndex = {
  1: 'admin',
  2: 'partner'
};

const roleName = {
  admin: 1,
  partner: 2
};

export default {
  role,
  roleIndex,
  roleName
};
