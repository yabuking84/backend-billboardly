const env = {
  foo: 'test',
  apiUrl: '192.168.1.200:5000',
  devMode: true
};

export default {
  ...env
};
