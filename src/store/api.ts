import axios from 'axios';
import cnfg from '@/config';

const base_url = cnfg.app.apiUrl;

export const bblyApi = axios.create({
    baseURL: base_url
});


export function setJWT() {
    bblyApi.defaults.headers.token = `${localStorage.getItem('access_token')}`;
}


export function clearJWT() {
    delete bblyApi.defaults.headers.token;
}