import cnfg from '@/config';

const state = {
  barColor: 'rgba(0, 0, 0, .8), rgba(0, 0, 0, .8)',
  barImage: '',
  drawer: true
};

const mutations = {
  SET_DRAWER_M(state: any, payload: any) {
    state.drawer = payload;
  }
};

export default {
  namespaced: true,
  state,
  mutations
  //   actions
};
