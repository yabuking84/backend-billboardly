interface State {
    api: Api;    
    token: string | null;
    axios: AxiosCnfg;
    authUser: AuthUser;
}

interface  Api {
    login: LoginOrOut;
    logout: LoginOrOut;
};


interface AxiosCnfg {
    config: Config;
}

interface Config {
    headers: Headers;
}

interface Headers {
    Authorization: string;
}


interface  LoginOrOut {
    method: string | null;
    url: string | null;
};

interface AuthUser {
    name: string | null;
    firstname: string | null;
    lastname: string | null;
    email: string | null;
    avatar: string | null;
    role: string | null;
    uuid: string | null;
    job_title: string | null;
    phone: string | null;
    fax: string | null;
    address: string | null;
    country_id: string | null;
}