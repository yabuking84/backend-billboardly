import axios, { Method }  from 'axios';
import { Api, User } from '@/types/models';
import cnfg from '@/config';

const base_url = cnfg.app.apiUrl;

const api = {
  registration: <Api> {
    method: 'post',
    url: `${base_url}/v1/register`
  }
}; 


const actions = {
  async partnerRegistration_a(context: any, user: User) {
    try {
      const rspns = await axios({
        method: <Method> api.registration.method,
        url: api.registration.url,
        headers: { token: localStorage.access_token }
      });
      return rspns;
    } catch(error) {
      console.error(error);
      return error;
    }
  }
};

const getters = {};

export default {
  namespaced: true,
  getters,
  actions
};

