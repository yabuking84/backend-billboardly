import axios from 'axios';
import { Method } from 'axios';
import {Auth_user, Api} from "@/types/models";

// import stateCnfg from "./State";

import cnfg from '@/config';

const base_url = cnfg.app.apiUrl;



const api = {
  login: <Api> {
    method: 'post',
    url: `${base_url}/v1/login`
  },
  logout: <Api> {
    method: 'post',
    url: `${base_url}/v1/logout`,
  },
};


const state = {
  token: localStorage.getItem('access_token') || null,
  auth_user: <Auth_user> {
    name: localStorage.getItem('name') || null,
    firstname: localStorage.getItem('firstname') || null,
    lastname: localStorage.getItem('lastname') || null,
    email: localStorage.getItem('email') || null,
    avatar: localStorage.getItem('avatar') || null,
    role: localStorage.getItem('role') || null,
    uuid: localStorage.getItem('uuid') || null,
    job_title: localStorage.getItem('job_title') || null,
    phone: localStorage.getItem('phone') || null,
    fax: localStorage.getItem('fax') || null,
    address: localStorage.getItem('address') || null,
    country_id: localStorage.getItem('country_id') || null,    
  }
};

const actions = {

  logout_a(context: any) {
    // console.log("headers");
    // console.log(headers);
    // console.log(localStorage.getItem('access_token'));

    // if(context.getters.isLoggedIn_g) {
    return new Promise((resolve, reject) => {
      axios({
        method: <Method> api.logout.method,
        url: api.logout.url,
        headers: { token: localStorage.access_token }
      })
        .then((response) => {
          // localStorage.removeItem('access_token');
          localStorage.clear();
          context.commit('DESTROY_TOKEN_M');
          context.commit('DESTROY_AUTHUSER_M');

          // reset notifications
          context.dispatch('ntfctns/resetAllNotification_a', null, { root: true });

          // unsubscribe to socket
          context.dispatch('sckts/unsubscribeSocket_a', null, { root: true });

          resolve(response);
        })
        .catch((error) => {
          localStorage.clear();
          context.commit('DESTROY_TOKEN_M');
          context.commit('DESTROY_AUTHUSER_M');

          // reset notifications
          context.dispatch('ntfctns/resetAllNotification_a', null, { root: true });

          // unsubscribe to socket
          context.dispatch('sckts/unsubscribeSocket_a', null, { root: true });

          reject(error);
        });
    });
    // }
  }


};

const getters = {
  isLoggedIn_g(state: { token: null }) {
    return state.token !== null;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  //   mutations,
  actions
};
