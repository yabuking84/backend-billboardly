import Vue from 'vue';
import Vuex, {Module} from 'vuex';

import auth from './modules/Auth';
import theme from './modules/Theme';

import registration from './modules/Partner/Registration'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  getters: {},
  actions: {},
  modules: {
    theme,
    auth,
    partner: {
      namespaced: true,
      modules: {
        registration
      }
    }
  }
});
