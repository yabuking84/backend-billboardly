declare module '*.vue' {
  import Vue from 'vue';

  export default Vue;
}
declare module 'vuetify/lib';
declare module 'vuetify/lib/locale/ar';
declare module 'vuetify/lib/locale/en';
