import Vue from 'vue'
import Component from 'vue-class-component'

// You can declare a mixin as the same style as components.
@Component
export default class Test extends Vue {
    test2(){
        return 'test2';
    };

}