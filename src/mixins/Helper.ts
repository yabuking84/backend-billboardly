// import axios from 'axios';
// import { ceil,floor } from 'lodash'

import Vue from 'vue'
import Component from 'vue-class-component'
import config from '@/config/index';

import {Role} from '@/types/models';

// You can declare a mixin as the same style as components.
@Component
export default class Helper extends Vue {

	    getDateTime(format = "yyyy-mm-dd", date: string){
	        let retVal: Date | string | null = null;

	        let d: Date | null = null;

	        const month = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

	        if(date)
	        d = new Date(date);
	        else
	        d = new Date();

	        if(format === "yyyy-mm-dd") {
	            retVal = new Date(
	                    Date.UTC(
	                          d.getFullYear(),
	                          d.getMonth(),
	                          d.getDate(),
	                          d.getHours(),
	                          d.getMinutes(),
	                          d.getSeconds()
	                    )
	              // `toIsoString` returns something like "2017-08-22T08:32:32.847Z"
	              // and we want the first part ("2017-08-22")
	              ).toISOString().slice(0, 10);
	        } 
	        else if(format === "mmm dd, yyyy") {
	            retVal = `${month[d.getMonth()]} ${d.getDate()}, ${d.getFullYear()}`;
	        } 
	        else if(format === "mmm dd, yyyy hh:mm") {
	            const hrs = (d.getHours()<10?"0":"")+d.getHours();
	            retVal = `${month[d.getMonth()]} ${d.getDate()}, ${d.getFullYear()} ${hrs}:${d.getMinutes()<10?"0":""}${d.getMinutes()}`;
	        } 
	        else {
	            retVal = null;
	        }


	        return retVal;
	    };

	    ucwords(str: string){
	        const string = str.trim();
	        if(string !== "")
	        return string.replace(/^./, string[0].toUpperCase());
	    };



	    isRole(roleName: 'admin'|'partner'){

	        let retVal = false;

	        if(roleName === 'admin') {
	            retVal = (localStorage.role === config.auth.role.admin.id);
	        }
	        else if(roleName === 'partner') {
	            retVal = (localStorage.role === config.auth.role.partner.id);
	        }

	        return retVal;
 
	    };


	    getRole(){
			const roleId: Role =  this.$store.state.auth.auth_user.role as Role;
	        return {
	        	id: roleId,
	        	name: config.auth.roleIndex[roleId],
	        };
	    };


	    getStore(type='inq'){

	    	console.log('$route',this.$route.meta.storeType[type]);

	        // return this.$route.meta.storeType.inq;
	        return this.$route.meta.storeType[type];
	    };



		currency(argParam: number,decimals=2){

			let arg = argParam;

			if(typeof arg === 'undefined')
			arg = 0.0;
			else if(!arg)
			arg = 0.0;

			// console.log('arg 2',arg);

			return this.numberWithCommas(arg.toFixed(decimals));
		};

		decimals(argParam: any,decimals=2){
			const retVal  = argParam;
			// return ceil(arg,2).toFixed(2)
			// return this.numberWithCommas(arg.toFixed(2));
			if(retVal)
			return parseFloat(retVal).toFixed(decimals);

			return 0;
		};


		numberWithCommas(x: string) {
		    
			if(typeof x === 'undefined')  {
				return '';
			}
		
			const parts = x.toString().split(".");
			parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			return parts.join(".");
			
		};


		cnsl(output1: string,output2=null){

			if(config.app.devMode) {
				if(output2)
				console.log(output1,output2);
				else
				console.log(output1);
			}
		};



		isIE() {
		  const ua = navigator.userAgent;
		  /* MSIE used to detect old browsers and Trident used to newer ones */
		  const is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
		  
		  return is_ie; 
		};


		// Changes XML to JSON
		xmlToJson(xmlArg: {}) {

			const xml = xmlArg! as any;
			// Create the return object
			let obj:any = [];

			if (xml.nodeType === 1) { // element
				// do attributes
				if (xml.attributes.length > 0) {
				obj["@attributes"] = {};
					for (let j = 0; j < xml.attributes.length; j++) {
						const attribute = xml.attributes.item(j);
						obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
					}
				}
			} else if (xml.nodeType === 3) { // text
				obj = xml.nodeValue;
			}

			// do children
			if (xml.hasChildNodes()) {
				for(let i = 0; i < xml.childNodes.length; i++) {
					const item = xml.childNodes.item(i);
					if (typeof(obj[item.nodeName]) === "undefined") {
						obj[item.nodeName] = this.xmlToJson(item);
					} else {
						if (typeof(obj[item.nodeName].push) === "undefined") {
							const old = obj[item.nodeName];
							obj[item.nodeName] = [];
							obj[item.nodeName].push(old);
						}
						obj[item.nodeName].push(this.xmlToJson(item));
					}
				}
			}
			return obj;
		};



		
		test(){
			return 'test';
		};


}