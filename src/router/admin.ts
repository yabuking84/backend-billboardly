import CoreAppBar from '@/views/main/components/core/AppBa.vue';
import CoreDrawer from '@/views/main/components/core/Drawe.vue';
import CoreFooter from '@/views/main/components/core/Foote.vue';

import AdminDashboard from '@/views/main/Index.vue';

export default [
  {
    name: 'AdminHome',
    path: '/dashboard',
    components: {
      default: AdminDashboard,
      appbar: CoreAppBar,
      drawer: CoreDrawer,
      footer: CoreFooter
    }
  }
];
