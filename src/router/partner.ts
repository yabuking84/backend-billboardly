/* eslint-disable */
import CoreAppBar from '@/views/main/components/core/AppBar.vue';
import CoreDrawer from '@/views/main/components/core/Drawer.vue';
import CoreFooter from '@/views/main/components/core/Footer.vue';

import PartnerDashboard from '@/views/main/Dashboard.vue';
import cnfg from '@/config'

const meta = {
  requiresAuth: true,
	role: cnfg.auth.role.partner.id
}

export default [
  {
    name: 'Dashboard',
    path: '/dashboard',
    components: {
      default: PartnerDashboard,
      appbar: CoreAppBar,
      drawer: CoreDrawer,
      footer: CoreFooter
    },
		meta: meta,
  }
];
