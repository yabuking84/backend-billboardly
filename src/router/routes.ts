/* eslint-disable */ 
/* eslint-disable import/named */
/* eslint-disable import/extensions */

import IndexPages from '@/views/pages/Index.vue';
import Login from '@/views/pages/Login.vue';
import Logout from '@/views/pages/Logout.vue';

import Register from '@/views/pages/Register.vue';

import Error from '@/views/pages/Error.vue';

import AdminRoutes from '@/router/admin';
import PartnerRoutes from '@/router/partner';

// This is needed because some routes of each of these users have the same path, there will conflict
// let userRoutes = [];
// if (store.state.auth.auth_user.role == config.auth.role.admin.id) {
//   userRoutes = AdminRoutes;
// } else if (store.state.auth.auth_user.role == config.auth.role.partner.id) {
//   userRoutes = PartnerRoutes;
// }

let userRoutes = PartnerRoutes;



const routes = [
  {
    name: 'Login',
    path: '/login',
    component: Login
  },
  {
    name: 'Logout',
    path: '/logout',
    component: Logout
  },
  {
    name: 'Register',
    path: '/register',
    component: Register
  },
  ...userRoutes,
  {
    path: '*',
    component: IndexPages,
    children: [
      {
        name: '404 Error',
        path: '',
        component: Error
      }
    ]
  } 
];

export default routes;
