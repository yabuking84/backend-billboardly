module.exports = {
  root: true,

  env: {
    node: true
  },

  plugins: [],

  // extends: [
  //   // "plugin:vue/essential",
  //   // "plugin:es-beautifier/standard",

  //   '@vue/typescript',
  //   'plugin:vue/essential', // this is a default sub-set of rules for your .vue files
  //   '@vue/airbnb', // plug airbnb rules but made for .vue files
  // ],

  extends: [
    'plugin:vue/essential', // this is a default sub-set of rules for your .vue files
    '@vue/airbnb', // plug airbnb rules but made for .vue files
    '@vue/typescript', // default typescript related rules
    'plugin:prettier/recommended'
  ],

  rules: {
    'no-console': 'off',
    'no-debugger': 'off',
    singlequote: 'off',
    // tawing
    'no-underscore-dangle': 'off',
    'global-require': 'off',
    'import/no-unresolved': 'off',
    'import/extensions': 'off',
    'no-plusplus': 'off',
    'consistent-return': 'off',
    'no-return-assign': 'off',
    'no-dupe-keys': 'off',
    'no-alert': 'off',
    'func-names': 'off',
    'import/no-extraneous-dependencies': 'off',
    'camelcase': 'off',
    'object-shorthand': 'off',
    'no-shadow': 'off',
    'class-methods-use-this': 'off',
    'eqeqeq': 'off',
    'no-restricted-globals': 'off',

    // test remove this!!!!
    'prettier/prettier': 'off',
    'import/no-duplicates': 'off',
    'import/no-named-as-default': 'off',
    'import/no-named-as-default-member': 'off',
    'no-trailing-spaces': 'off',
    'no-mixed-spaces-and-tabs': 'off',
    'no-multiple-empty-lines': 'off',
    'no-tabs': 'off'
  },

  parserOptions: {
    parser: '@typescript-eslint/parser',
    sourceType: 'module', // allow the use of imports statements
    ecmaVersion: 2018 // allow the parsing of modern ecmascript
  }
};
